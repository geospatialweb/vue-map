## Geospatial Web

http://www.geospatialweb.ca

My sample website to showcase the code integration of Node, Express, Vue, Vuex, Socket IO, Mapbox GL, PostGIS and Docker. The code repository above is production build code configured for Docker Compose instances.

 If you wish to serve locally, email me at johncampbell@geospatialweb.ca for the .env file.
 
 ```git clone https://gitlab.com/geospatialweb/vue-map.git```

 ```docker-compose up -d```
